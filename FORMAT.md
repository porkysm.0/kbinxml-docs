# KBinXML Format
This document is incomplete and may not cover everything.

As this was created by examining https://github.com/Kirito3481/kbinxml-sharp, there may be some inconsistencies with other implementations.

## Header
All header data is aligned linearly from 0x0.

| Size             | Type        | Description                                              |
|------------------|-------------|----------------------------------------------------------|
| 0x1              | Byte        | KBinXML Signature (Always 0xA0)                          |
| 0x1              | Byte        | Compression Boolean (0x42 if compressed, otherwise 0x45) |
| 0x1              | Byte        | [Encoding Type](#encoding-types) (>> 5 for encoding type index)             |
| 0x1              | ?           | ?                                                        |
| 0x4              | Uint32      | Node Buffer Size                                         |
| Node Buffer Size | Node Buffer | Node Buffer                                              |
| 0x4              | Uint32      | Uint32 Data Buffer Size                                  |
| Data Buffer Size | Data Buffer | Data Buffer                                              |

## Buffers
### Data Buffer
The Data Buffer is read alongside the Node Buffer for additional data about an event.

## Miscellaneous Structures
### NodeBuffer Uncompressed String
| Size               | Type   | Description |
|--------------------|--------|-------------|
| 0x1                | Uint8  | Length      |
| (Length & ~64) + 1 | Binary | Data        |

## NodeBuffer Compressed String
| Size                   | Type   | Description |
|------------------------|--------|-------------|
| 0x1                    | Uint8  | Length      |
| ceil(Length * 6 / 8.f) | Compressed Binary | Data        |

## Events
The next event code can be read by reading the next byte from the Node Buffer. This code can be translated into an [event index](#event-types) using:

```event_code & ~(1 << 6)```


### NodeStart
When a NodeStart event is encountered, the current node should be set to a new node with the given name. 

**Node Buffer**

| Size     | Type   | Description |
|----------|--------|-------------|
| Variable | [String](#nodebuffer-uncompressed-string) | Name        |

### Attribute
When an Attribute event is encountered, the new attribute with the given properties should be added to the current node.

**Node Buffer**

| Size         | Type   | Description  |
|--------------|--------|--------------|
| Variable| [String](#nodebuffer-uncompressed-string)  | Name         |

**Data Buffer**

| Size         | Type   | Description  |
|--------------|--------|--------------|
| 0x4          | Uint32 | Value Length |
| Value Length | Binary | Value        |

### NodeEnd
When a NodeEnd event is encountered, the current node should be set to its parent.

### FileEnd
The FileEnd event signifies that the end of the file has been reached and the XML structure should be complete.

### Value Types
Any Value event will create a new child element to the current node with the following XML template:

```xml
<{node name} __type="{type}">{value}</{node_name}>
```

TODO

## Encoding Types
| Index | Encoding Type |
|-------|---------------|
| 0     | SHIFT_JIS     |
| 1     | ASCII         |
| 2     | ISO-8859-1    |
| 3     | EUC-JP        |
| 4     | SHIFT_JIS     |
| 5     | UTF-8         |

## Event Types
| Index | Event Type|
|-----|------------|
| 1   | Node Start |
| 2   | Type s8    |
| 3   | Type u8    |
| 4   | Type s16   |
| 5   | Type u16   |
| 6   | Type s32   |
| 7   | Type u32   |
| 8   | Type s64   |
| 9   | Type u64   |
| 10  | Type bin   |
| 11  | Type str   |
| 12  | Type ip4   |
| 13  | Type time  |
| 14  | Type float |
| 15  | Type double|
| 16  | Type 2s8   |
| 17  | Type 2u8   |
| 18  | Type 2s16   |
| 19  | Type 2u16   |
| 20  | Type 2s32   |
| 21  | Type 2u32   |
| 22  | Type 2s64   |
| 23  | Type 2u64   |
| 24  | Type 2f   |
| 25  | Type 2d   |
| 26  | Type 3s8   |
| 27  | Type 3u8   |
| 28  | Type 3s16   |
| 29  | Type 3u16   |
| 30  | Type 3s32   |
| 31  | Type 3u32   |
| 32  | Type 3s64   |
| 33  | Type 3u64   |
| 34  | Type 3f   |
| 35  | Type 3d   |
| 36  | Type 4s8   |
| 37  | Type 4u8   |
| 38  | Type 4s16   |
| 39  | Type 4u16   |
| 40  | Type 4s32   |
| 41  | Type 4u32   |
| 42  | Type 4s64   |
| 43  | Type 4u64   |
| 44  | Type 4f   |
| 45  | Type 4d   |
| 46  | Attribute   |
| 47  | Type array (?)   |
| 48  | Type vs8  |
| 49  | Type vu8  |
| 50  | Type vs16  |
| 51  | Type vu16  |
| 52  | Type bool  |
| 53  | Type 2b  |
| 54  | Type 3b  |
| 55  | Type 4b  |
| 56  | Type vb  |
| 190 | NodeEnd    |
| 191 | FileEnd    |